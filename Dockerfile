FROM bitnami/rabbitmq:3.9

COPY rootfs /
COPY docker-entrypoint-hooks.sh /
COPY on-startup.sh /hooks/on-startup.sh
#USER root
#RUN chown 1001:1001 -R /bitnami
#USER 1001
ENTRYPOINT ["/docker-entrypoint-hooks.sh"]
