#!/usr/bin/env bash
set -e
# Specific commands for Application
# rabbitmq-plugins enable rabbitmq_web_stomp
# Specific commands for Application
rabbitmqctl add_user $RABBITMQ_WS_USER $RABBITMQ_WS_PASS
rabbitmqctl add_vhost $RABBITMQ_WS_VHOST
rabbitmqctl set_permissions -p $RABBITMQ_WS_VHOST $RABBITMQ_WS_USER ".*" ".*" ".*"
rabbitmqctl set_permissions -p $RABBITMQ_WS_VHOST $RABBITMQ_USERNAME ".*" ".*" ".*"